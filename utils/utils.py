from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import os,time
import test_config





def Test_setUp(self):
    pass
#     self.browser = webdriver.Firefox()
#     self.browser.get(test_config.pageToOpen) 
 

def Test_tearDown(self) :
    self.browser.quit()



def get_element(self,full_locator):
    self.browser = webdriver.Firefox()
    br = self.browser.get(test_config.pageToOpen) 
 
   
    locator_type = full_locator[1].lower
    locator = full_locator[0]
    if locator_type == 'id':
        for i in range(2):
            try:
                if br.find_element_by_id(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_by_id(locator)
            time.sleep(0.02)
    elif locator_type == 'class' or locator_type == "class_name":
        for i in range(2):
            try:
                if br.find_element_by_class_name(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_by_class_name(locator)
            time.sleep(0.02)
    elif locator_type == 'css' or locator_type == "css_selector":
        for i in range(2):
            try:
                if br.find_element_by_css_selector(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_by_css_selector(locator)
            time.sleep(0.02)
    elif locator_type == 'link' or locator_type == "link_text":
        for i in range(2):
            try:
                if br.find_element_by_link_text(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_by_link_text(locator)
            time.sleep(0.02)
    elif locator_type == 'p_link' or locator_type == "partial_link_text":
        for i in range(2):
            try:
                if br.find_element_by_partial_link_text(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_by_partial_link_text(locator)
            time.sleep(0.02)
    elif locator_type == 'name':
        for i in range(2):
            try:
                if br.find_element_by_name(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_by_name(locator)
            time.sleep(0.02)
    elif locator_type == 'tag' or locator_type == 'tag_name':
        for i in range(2):
            try:
                if br.find_element_by_tag_name(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_tag_by_name(locator)
            time.sleep(0.02)
    elif locator_type == 'xpath':
        for i in range(2):
            try:
                if br.find_element_by_xpath(locator).is_displayed():
                    break
            except:
                time.sleep(1)
                pass
            return  br.find_element_by_xpath(locator)
            time.sleep(0.02)